{
  const {
    html,
  } = Polymer;
  /**
    `<reciever-prueba1>` Description.

    Example:

    ```html
    <reciever-prueba1></reciever-prueba1>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --reciever-prueba1 | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class RecieverPrueba1 extends Polymer.Element {

    static get is() {
      return 'reciever-prueba1';
    }

    static get properties() {
      return {};
    }

    static get template() {
      return html `
      <style include="reciever-prueba1-styles reciever-prueba1-shared-styles"></style>
      <slot></slot>
      
          <p>Welcome to Cells</p>
      
      `;
    }
  }

  customElements.define(RecieverPrueba1.is, RecieverPrueba1);
}